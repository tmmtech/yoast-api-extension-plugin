<?php
/**
 * Defines plugin constants.
 *
 * @package Yoast-API-Extension
 */

$plugin_file = realpath( __DIR__ . '/../yoast-api-extension.php' );

/*
 * The URL to the plugin.
 */
define( 'YOAST_API_EXTENSION_URL', plugin_dir_url( $plugin_file ) );

/*
 * The filesystem directory path to the plugin.
 */
define( 'YOAST_API_EXTENSION_DIR', plugin_dir_path( $plugin_file ) );

/*
 * The version of the plugin.
 */
define( 'YOAST_API_EXTENSION_VERSION', get_file_data( $plugin_file, [ 'Version' ] )[0] );

/*
 * The filename of the plugin including the path.
 */
define( 'YOAST_API_EXTENSION_FILE', $plugin_file );

/*
 * Plugin basename.
 */
define( 'YOAST_API_EXTENSION_BASENAME', plugin_basename( YOAST_API_EXTENSION_FILE ) );
