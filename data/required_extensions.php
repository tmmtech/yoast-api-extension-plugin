<?php
/**
 * A list of required PHP extensions autogenerated by the build script.
 *
 * @package Yoast
 */

$required_extensions = [
    'mbstring',
];
