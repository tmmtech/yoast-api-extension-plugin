<?php
/**
 * Plugin Name: Yoast API Extension
 * Description: Extends the default Yoast API.
 * Version:     0.1.13
 * Text Domain: yoast-api-extension
 * Author:      TMM Technology
 * Author URI:  https://tmm.ventures/
 * Requires PHP: 7.4
 * License:     GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package Yoast-API-Extension
 */

namespace Yoast_API_Extension;

/**
 * Main Yoast_API_Extension class.
 */
final class Yoast_API_Extension {

    /**
     * File System instance.
     *
     * @var \WP_Filesystem_Base
     */
    public static $fs;

    /**
     * Fields.
     *
     * @var Fields
     */
    public static $fields;

    /**
     * Plugin slug.
     *
     * @var string
     */
    public static $slug;

    /**
     * Plugin version.
     *
     * @var string
     */
    public static $version;

    /**
     * Plugin name.
     *
     * @var string
     */
    public static $name;

    /**
     * Constructor.
     */
    public function __construct() {
        $this->define_constants();
        $this->import_initial_files();
        $this->init_filesystem_class();

        self::$fs = $GLOBALS['wp_filesystem'];

        add_action(
            'plugins_loaded',
            function() {
                // Load translations.
                $this->load_plugin_textdomain();

                $dependencies = new Dependencies\Dependencies();
                $dependencies->add();
                if ( $dependencies->are_plugins_installed() && $dependencies->are_plugins_active() ) {
                    $this->import_plugin_files();

                    new \WP_Plugins_Core\WP_Plugins_Core( $this );

                    // Adds plugin settings links to plugins admin screen.
                    add_filter( 'plugin_action_links_' . YOAST_API_EXTENSION_BASENAME, [ $this, 'plugin_action_links' ] );

                    add_action(
                        'init',
                        function () {
                            self::$fields = new Fields();

                            // Init REST API.
                            new REST_API();

                            // Init Yoast extensions.
                            new Yoast_Extensions\Init();

                            // Menu.
                            new Admin_Menu();
                        }
                    );
                }
            }
        );
    }

    /**
     * Defines constants.
     */
    private function define_constants() {
        require_once __DIR__ . '/data/constants.php';

        /**
         * Plugin name.
         */
        self::$name = get_file_data( YOAST_API_EXTENSION_FILE, [ 'Plugin Name' ] )[0];

        /**
         * Plugin slug.
         */
        $dir_parts  = explode( '/', YOAST_API_EXTENSION_DIR );
        self::$slug = $dir_parts[ array_search( 'plugins', $dir_parts, true ) + 1 ];

        /**
         * Plugin version.
         */
        self::$version = YOAST_API_EXTENSION_VERSION;
    }

    /**
     * Imports initial plugin files:
     *
     * - For dependencies check.
     * - And WP plugins core.
     */
    private function import_initial_files() {
        $src_files = [
            'dependencies/interfaces/interface-dependency',
            'dependencies/interfaces/abstract-class-dependency',
            'dependencies/class-wc',
            'dependencies/class-yoast',
            'dependencies/class-dependencies',
        ];
        foreach ( $src_files as $file ) {
            require_once YOAST_API_EXTENSION_DIR . 'src/' . $file . '.php';
        }

        $files = [
            'vendor/autoload_packages',
            'vendor/tmmtech/wp-plugins-core/wp-plugins-core',
            'vendor/woocommerce/action-scheduler/action-scheduler',
        ];
        foreach ( $files as $file ) {
            require_once YOAST_API_EXTENSION_DIR . $file . '.php';
        }
    }

    /**
     * Imports plugin files.
     */
    private function import_plugin_files() {
        $src_files = [
            'assets/screens/class-assets-main-screen',
            'class-admin-menu',
            'class-fields',
            'class-post-data',
            'rest-api/class-rest-api',
            'rest-api/controllers/interfaces/interface-rest-endpoint',
            'rest-api/controllers/class-set-post-fields',
            'yoast-extensions/class-extensions-init',
            'yoast-extensions/interfaces/interface-extension-init',
            'yoast-extensions/interfaces/abstract-class-extension-init',
            'yoast-extensions/woocommerce/class-init',
        ];
        foreach ( $src_files as $file ) {
            require_once YOAST_API_EXTENSION_DIR . 'src/' . $file . '.php';
        }

        $files = [
            'vendor/autoload_packages',
            'vendor/tmmtech/wp-plugins-core/wp-plugins-core',
            'vendor/woocommerce/action-scheduler/action-scheduler',
        ];
        foreach ( $files as $file ) {
            require_once YOAST_API_EXTENSION_DIR . $file . '.php';
        }
    }

    /**
     * Loads textdomain.
     */
    private function load_plugin_textdomain() {
        load_plugin_textdomain(
            'yoast-api-extension',
            false,
            dirname( YOAST_API_EXTENSION_BASENAME ) . '/languages'
        );
    }

    /**
     * Inits filesystem class, which has to be declared in $wp_filesystem global variable.
     */
    private function init_filesystem_class() {
        if ( ! function_exists( 'WP_Filesystem' ) ) {
            require_once ABSPATH . 'wp-admin/includes/file.php';
        }

        if ( ! class_exists( 'WP_Filesystem_Base' ) ) {
            WP_Filesystem();
        }
    }

    /**
     * Show settings link on the plugin screen.
     *
     * @param mixed $links Plugin Action links.
     *
     * @return array
     */
    public function plugin_action_links( $links ) {
        $action_links = array(
            'settings' => '<a href="' . admin_url( 'admin.php?page=yoast-api-extension' ) . '" aria-label="' .
                esc_attr__( 'View Yoast API Extension documentation', 'yoast-api-extension' ) . '">' . esc_html__( 'Documentation', 'yoast-api-extension' ) . '</a>',
        );
        return array_merge( $action_links, $links );
    }
}
new Yoast_API_Extension();
