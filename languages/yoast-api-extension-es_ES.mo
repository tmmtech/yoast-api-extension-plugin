��          �      \      �  )   �  .   �     *  ?   >  G   ~     �     �     �     �  (   �  8   (     a     s     z     �  %   �  &   �     �  !   �  s     ,   �  5   �     �  V     L   e     �     �  (   �     �  .   �  @   '     h     z     �     �  *   �  ,   �     	  2   !                                                                
   	                                1. Create a product with the identifiers: 2. Modify identifier for the existing product: Basic documentation Can't find image ID. Are you using the image from this website? Command example to create a product with GTIN8 by specifying meta data. Data Documentation Extends the default Yoast API. Method Modify a field for the existing product: Product Identifiers — "Yoast SEO: WooCommerce" plugin. Supported fields: Syntax Unknown field: Unknown identifier: Value should be a string, but passed: View Yoast API Extension documentation Yoast API Extension Yoast API Extension Documentation Project-Id-Version: Yoast API Extension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-05-30 12:05+0200
Language-Team: 
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Loco-Version: 2.6.1; wp-5.9.3
X-Domain: yoast-api-extension
Last-Translator: 
Language: es
 1. Crea un producto con los identificadores: 2. Modifique el identificador del producto existente: Documentación básica No se puede encontrar el ID de imagen. ¿Está utilizando la imagen de este sitio web? Ejemplo de comando para crear un producto con GTIN8 especificando metadatos. Datos Documentación Extiende la API de Yoast predeterminada. Método Modificar un campo para el producto existente: Identificadores de producto — Plugin "Yoast SEO: WooCommerce". Campos admitidos: Sintaxis Campo desconocido: Identificador desconocido: El valor debe ser una cadena, pero pasado: Ver la documentación de Yoast API Extension Extensión API de Yoast Documentación de la extensión de la API de Yoast 