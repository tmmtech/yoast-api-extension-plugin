��          �      \      �  )   �  .   �     *  ?   >  G   ~     �     �     �     �  (   �  8   (     a     s     z     �  %   �  &   �     �  !   �  �     G   �  [   %  )   �  �   �     J     �     �  5   �  
   &  M   1  Z     &   �     	      	  2   5	  U   h	  S   �	     
  <   1
                                                                
   	                                1. Create a product with the identifiers: 2. Modify identifier for the existing product: Basic documentation Can't find image ID. Are you using the image from this website? Command example to create a product with GTIN8 by specifying meta data. Data Documentation Extends the default Yoast API. Method Modify a field for the existing product: Product Identifiers — "Yoast SEO: WooCommerce" plugin. Supported fields: Syntax Unknown field: Unknown identifier: Value should be a string, but passed: View Yoast API Extension documentation Yoast API Extension Yoast API Extension Documentation Project-Id-Version: Yoast API Extension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-05-30 12:07+0200
Language-Team: 
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Loco-Version: 2.6.1; wp-5.9.3
X-Domain: yoast-api-extension
Last-Translator: 
Language: ru
 1. Создайте продукт с идентификаторами: 2. Измените идентификатор существующего продукта: Основная документация Не удается найти идентификатор изображения. Вы используете изображение с этого сайта? Пример команды для создания продукта с GTIN8 путем указания метаданных. Данные Документация Расширяет API Yoast по умолчанию. Метод Измените поле для существующего продукта: Идентификаторы продуктов — плагин "Yoast SEO: WooCommerce". Поддерживаемые поля: Синтаксис Неизвестное поле: Неизвестный идентификатор: Значение должно быть строковым, но передается: Просмотреть документацию по расширению API Yoast Расширение API Yoast Документация по расширению API Yoast 