��          �      \      �  )   �  .   �     *  ?   >  G   ~     �     �     �     �  (   �  8   (     a     s     z     �  %   �  &   �     �  !   �  s     5   �  7   �       N     R   l     �     �  !   �     �  1   �  9   /     i          �     �  5   �  *   �       !   '                                                                
   	                                1. Create a product with the identifiers: 2. Modify identifier for the existing product: Basic documentation Can't find image ID. Are you using the image from this website? Command example to create a product with GTIN8 by specifying meta data. Data Documentation Extends the default Yoast API. Method Modify a field for the existing product: Product Identifiers — "Yoast SEO: WooCommerce" plugin. Supported fields: Syntax Unknown field: Unknown identifier: Value should be a string, but passed: View Yoast API Extension documentation Yoast API Extension Yoast API Extension Documentation Project-Id-Version: Yoast API Extension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-05-30 12:05+0200
Language-Team: 
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Loco-Version: 2.6.1; wp-5.9.3
X-Domain: yoast-api-extension
Last-Translator: 
Language: de
 1. Erstellen Sie ein Produkt mit den Identifikatoren: 2. Ändern Sie die Kennung für das vorhandene Produkt: Grundlegende Dokumentation Bild-ID kann nicht gefunden werden. Verwenden Sie das Bild von dieser Website? Befehlsbeispiel zum Erstellen eines Produkts mit GTIN8 durch Angabe von Metadaten. Daten Dokumentation Erweitert die Yoast-Standard-API. Methode Ändern Sie ein Feld für das vorhandene Produkt: Produktidentifikatoren - "Yoast SEO: WooCommerce" Plugin. Unterstützte Felder: Syntax Unbekanntes Feld: Unbekannter Bezeichner: Value sollte eine Zeichenfolge sein, aber übergeben: Yoast API Extension-Dokumentation anzeigen Yoast API-Erweiterung Yoast API Extension Dokumentation 