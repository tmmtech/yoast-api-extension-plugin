��          �      \      �  )   �  .   �     *  ?   >  G   ~     �     �     �     �  (   �  8   (     a     s     z     �  %   �  &   �     �  !   �  r     *   �  2   �     �  N     K   W     �     �     �     �  ,   �  @   
     K     ^     f     z  ,   �  4   �     �  (                                                                   
   	                                1. Create a product with the identifiers: 2. Modify identifier for the existing product: Basic documentation Can't find image ID. Are you using the image from this website? Command example to create a product with GTIN8 by specifying meta data. Data Documentation Extends the default Yoast API. Method Modify a field for the existing product: Product Identifiers — "Yoast SEO: WooCommerce" plugin. Supported fields: Syntax Unknown field: Unknown identifier: Value should be a string, but passed: View Yoast API Extension documentation Yoast API Extension Yoast API Extension Documentation Project-Id-Version: Yoast API Extension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-05-30 12:07+0200
Language-Team: 
Plural-Forms: nplurals=2; plural=(n > 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Loco-Version: 2.6.1; wp-5.9.3
X-Domain: yoast-api-extension
Last-Translator: 
Language: pt
 1. Crie um produto com os identificadores: 2. Modificar o identificador do produto existente: Documentação básica Não consigo encontrar a ID de imagem. Você está usando a imagem deste site? Exemplo de comando para criar um produto com GTIN8 especificando metadados. Dados Documentação Estende a API Yoast padrão. Método Modifique um campo para o produto existente: Identificadores de produtos — plugin "Yoast SEO: WooCommerce". Campos suportados: Sintaxe Campo desconhecido: Identificador desconhecido: O valor deve ser uma sequência, mas passou: Exibir a documentação da extensão da API do Yoast Extensão yoast API Documentação de extensão da API yoast 