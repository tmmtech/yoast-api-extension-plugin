��          �      \      �  )   �  .   �     *  ?   >  G   ~     �     �     �     �  (   �  8   (     a     s     z     �  %   �  &   �     �  !   �  r     ,   �  1   �     �  O     W   X     �     �  !   �     �  ,   �  C        c     {     �     �  0   �  0   �       /   &                                                                
   	                                1. Create a product with the identifiers: 2. Modify identifier for the existing product: Basic documentation Can't find image ID. Are you using the image from this website? Command example to create a product with GTIN8 by specifying meta data. Data Documentation Extends the default Yoast API. Method Modify a field for the existing product: Product Identifiers — "Yoast SEO: WooCommerce" plugin. Supported fields: Syntax Unknown field: Unknown identifier: Value should be a string, but passed: View Yoast API Extension documentation Yoast API Extension Yoast API Extension Documentation Project-Id-Version: Yoast API Extension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-05-30 12:06+0200
Language-Team: 
Plural-Forms: nplurals=2; plural=(n > 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Loco-Version: 2.6.1; wp-5.9.3
X-Domain: yoast-api-extension
Last-Translator: 
Language: fr
 1. Créez un produit avec les identifiants : 2. Modifier l’identifiant du produit existant : Documentation de base Impossible de trouver l’ID d’image. Utilisez-vous l’image de ce site Web? Exemple de commande pour créer un produit avec GTIN8 en spécifiant des métadonnées. Données Documentation Étend l’API Yoast par défaut. Méthode Modifiez un champ pour le produit existant : Identificateurs de produit - Plugin « Yoast SEO: WooCommerce ». Champs pris en charge : Syntaxe Champ inconnu : Identifiant inconnu : La valeur doit être une chaîne, mais passée : Afficher la documentation de Yoast API Extension Extension de l’API Yoast Documentation de l’extension de l’API Yoast 