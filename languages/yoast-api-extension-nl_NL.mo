��          �      \      �  )   �  .   �     *  ?   >  G   ~     �     �     �     �  (   �  8   (     a     s     z     �  %   �  &   �     �  !   �  s         �  +   �     �  J   �  R   >     �     �  ,   �     �  -   �  8   
     C     X     a     p  2   �  &   �     �      �                                                                
   	                                1. Create a product with the identifiers: 2. Modify identifier for the existing product: Basic documentation Can't find image ID. Are you using the image from this website? Command example to create a product with GTIN8 by specifying meta data. Data Documentation Extends the default Yoast API. Method Modify a field for the existing product: Product Identifiers — "Yoast SEO: WooCommerce" plugin. Supported fields: Syntax Unknown field: Unknown identifier: Value should be a string, but passed: View Yoast API Extension documentation Yoast API Extension Yoast API Extension Documentation Project-Id-Version: Yoast API Extension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-05-30 12:06+0200
Language-Team: 
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Loco-Version: 2.6.1; wp-5.9.3
X-Domain: yoast-api-extension
Last-Translator: 
Language: nl
 1. Maak een product met de id's: 2. Wijzig de id voor het bestaande product: Basisdocumentatie Kan afbeeldings-ID niet vinden. Gebruikt u de afbeelding van deze website? Opdrachtvoorbeeld om een product met GTIN8 te maken door metagegevens op te geven. Gegevens Documentatie Hiermee breidt u de standaard Yoast API uit. Methode Een veld voor het bestaande product wijzigen: Product Identifiers — "Yoast SEO: WooCommerce" plugin. Ondersteunde velden: Syntaxis Onbekend veld: Onbekende identificatiecode: Waarde moet een tekenreeks zijn, maar is geslaagd: Bekijk yoast API extensie documentatie Yoast API-extensie Yoast API Extension Documentatie 