��          �      \      �  )   �  .   �     *  ?   >  G   ~     �     �     �     �  (   �  8   (     a     s     z     �  %   �  &   �     �  !   �  s     +   �  9   �     �  N     L   `     �     �      �     �  .   �  ?        X     j     s     �  .   �  3   �       -                                                                   
   	                                1. Create a product with the identifiers: 2. Modify identifier for the existing product: Basic documentation Can't find image ID. Are you using the image from this website? Command example to create a product with GTIN8 by specifying meta data. Data Documentation Extends the default Yoast API. Method Modify a field for the existing product: Product Identifiers — "Yoast SEO: WooCommerce" plugin. Supported fields: Syntax Unknown field: Unknown identifier: Value should be a string, but passed: View Yoast API Extension documentation Yoast API Extension Yoast API Extension Documentation Project-Id-Version: Yoast API Extension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-05-30 12:06+0200
Language-Team: 
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Loco-Version: 2.6.1; wp-5.9.3
X-Domain: yoast-api-extension
Last-Translator: 
Language: it
 1. Crea un prodotto con gli identificatori: 2. Modificare l'identificatore per il prodotto esistente: Documentazione di base Impossibile trovare l'ID immagine. Stai utilizzando l'immagine da questo sito? Esempio di comando per creare un prodotto con GTIN8 specificando i metadati. Dati Documentazione Estende l'API Yoast predefinita. Metodo Modificare un campo per il prodotto esistente: Identificatori di prodotto — Plugin "Yoast SEO: WooCommerce". Campi supportati: Sintassi Campo sconosciuto: Identificatore sconosciuto: Il valore deve essere una stringa, ma passato: Visualizza la documentazione di Yoast API Extension Estensione API Yoast Documentazione sull'estensione DELL'API Yoast 