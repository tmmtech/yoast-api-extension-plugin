��          �      \      �  )   �  .   �     *  ?   >  G   ~     �     �     �     �  (   �  8   (     a     s     z     �  %   �  &   �     �  !   �  �     &   �  /   �     $  M   <  P   �     �     �  (   �       .     ?   L     �  	   �     �     �  A   �  +        >  #   U                                                                
   	                                1. Create a product with the identifiers: 2. Modify identifier for the existing product: Basic documentation Can't find image ID. Are you using the image from this website? Command example to create a product with GTIN8 by specifying meta data. Data Documentation Extends the default Yoast API. Method Modify a field for the existing product: Product Identifiers — "Yoast SEO: WooCommerce" plugin. Supported fields: Syntax Unknown field: Unknown identifier: Value should be a string, but passed: View Yoast API Extension documentation Yoast API Extension Yoast API Extension Documentation Project-Id-Version: Yoast API Extension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-05-30 12:06+0200
Language-Team: 
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Loco-Version: 2.6.1; wp-5.9.3
X-Domain: yoast-api-extension
Last-Translator: 
Language: pl
 1. Utwórz produkt z identyfikatorami: 2. Zmień identyfikator istniejącego produktu: Podstawowa dokumentacja Nie można znaleźć identyfikatora obrazu. Czy używasz obrazu z tej strony? Przykład polecenia, aby utworzyć produkt z GTIN8 przez określenie metadanych. Dane Dokumentacja Rozszerza domyślny interfejs API Yoast. Metoda Modyfikowanie pola dla istniejącego produktu: Identyfikatory produktów — wtyczka "Yoast SEO: WooCommerce". Obsługiwane pola: Składnia Nieznane pole: Nieznany identyfikator: Wartość powinna być ciągiem znaków, ale została przekazana: Zobacz dokumentację rozszerzenia Yoast API Rozszerzenie Yoast API Dokumentacja rozszerzenia Yoast API 