== Changelog ==

= 0.1.13 2024-09-18 =

#### Bugfixes

* Use standard Action Scheduler interval.
* Update WP-Plugins-Core.

= 0.1.12 2024-05-02 =

#### Enhancements

* Use standard Action Scheduler interval.
* Update WP-Plugins-Core.

= 0.1.11 2023-07-18 =

#### Enhancements

* Set min PHP version to 7.4.

= 0.1.9 2023-06-10 =

#### Enhancements

* Decrease plugin size by removing unnecessary files.

= 0.1 2023-01-29 =

#### Enhancements

* Changelog added.
