require( 'dotenv' ).config({ path: './node.env', override: true });

const defaultConfig = require("@wordpress/scripts/config/webpack.config");
const path = require("path");

module.exports = {
  ...defaultConfig,
  entry: {
    'admin/index': './assets/admin/index.js',
    'admin/screens/main': './assets/admin/screens/main/index.js',
  },
  output: {
    ...defaultConfig.output,
    path: path.resolve( __dirname, 'assets-build' ),
  }
};
