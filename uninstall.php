<?php
/**
 * Yoast_API_Extension Uninstall
 *
 * Deletes Yoast_API_Extension options and other data.
 *
 * @package Yoast-API-Extension
 * @since 0.0.1
 */

// Security check.

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit;
}
