<?php
/**
 * Admin Menus
 *
 * Adds admin menus.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension;

/**
 * Class Admin_Menu.
 */
final class Admin_Menu {

    /**
     * Adds the menu and inits assets loading for it.
     */
    public function __construct() {
        add_action( 'admin_menu', [ $this, 'init_menu' ] );
    }

    /**
     * Adds the menu and inits assets loading for it.
     */
    public function init_menu() {
        $menu_slug = add_submenu_page(
            'wpseo_dashboard',
            __( 'Yoast API Extension', 'yoast-api-extension' ),
            __( 'Yoast API Extension', 'yoast-api-extension' ),
            'manage_options',
            'yoast-api-extension',
            function () {
                require_once YOAST_API_EXTENSION_DIR . 'src/templates/admin/screens/main.php';
            }
        );

        add_action(
            'load-' . $menu_slug,
            [ 'Yoast_API_Extension\Assets\Menu\Screens\Main', 'init' ]
        );
    }
}
