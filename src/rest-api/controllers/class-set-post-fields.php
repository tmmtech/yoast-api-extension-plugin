<?php
/**
 * Custom REST API Controller to set Yoast product fields.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension\REST_API_Controllers;

use Exception;
use WP_Error;
use WP_REST_Request;
use Yoast_API_Extension\Post_Data;
use Yoast_API_Extension\Yoast_API_Extension;

/**
 * Class Set_Post_Fields.
 */
class Set_Post_Fields implements REST_Endpoint {

    /**
     * Namespace.
     *
     * @var string
     */
    const REST_NAMESPACE = 'yoast-api-extension/v1';

    /**
     * Endpoint name.
     *
     * @var string
     */
    const REST_BASE = 'set_post_fields';

    /**
     * Post type (for which permissions check will be done).
     *
     * @var string
     */
    protected $post_type = 'product';

    /**
     * Register the routes.
     */
    public function register_routes() {
        register_rest_route(
            self::REST_NAMESPACE,
            '/' . self::REST_BASE . '/(?P<post_id>[\d-]+)',
            [
                'methods'             => 'PUT',
                'args'                => [
                    'fields'  => [
                        'required'    => true,
                        'type'        => 'object',
                        'description' => 'The type of the identifier to set.',
                    ],
                    'post_id' => [
                        'required'    => true,
                        'type'        => 'number',
                        'description' => 'The ID of the post to set the identifier for.',
                    ],
                ],
                'callback'            => [
                    $this,
                    'set_field',
                ],
                'permission_callback' => [
                    $this,
                    'get_items_permissions_check',
                ],
            ]
        );
    }

    /**
     * Check if a given request has access to read items.
     *
     * @return WP_Error|boolean
     */
    public function get_items_permissions_check() {
        if ( ! wc_rest_check_post_permissions( $this->post_type, 'create' ) ) {
            return new WP_Error(
                'woocommerce_rest_cannot_create',
                __( 'Sorry, you are not allowed to create resources.', 'woocommerce' ),
                [ 'status' => rest_authorization_required_code() ]
            );
        }
        return true;
    }

    /**
     * Sets the post field.
     *
     * @param WP_REST_Request $request Where the body is an JSON object with the following values:
     *
     *  fields  - The array of fields and their data.
     *  post_id - The ID of the post to set the identifier for.
     *
     * @return array
     *
     * @throws Exception Exception.
     */
    public function set_field( $request ) {
        $fields = $request->get_param( 'fields' );

        foreach ( $fields as $field => $value ) {
            if ( ! Yoast_API_Extension::$fields->yae_validate_field_name( $field ) ) {
                wp_die( __( 'Unknown field:', 'yoast-api-extension' ) . ' ' . $field );
            }
        }

        $post_id = $request->get_param( 'post_id' );

        $post_data = new Post_Data();

        foreach ( $fields as $field => $value ) {
            $post_data->set_post_data( $post_id, $field, $value );
        }

        return [];
    }
}
