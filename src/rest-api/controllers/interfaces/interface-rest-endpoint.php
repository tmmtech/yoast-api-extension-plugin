<?php
/**
 * Rest Endpoint interface.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension\REST_API_Controllers;

/**
 * Dictates the required methods for an Endpoint implementation.
 */
interface REST_Endpoint {

    /**
     * Registers the routes for the endpoints.
     */
    public function register_routes();

    /**
     * Permissions check.
     *
     * @return bool Whether the endpoint use it allowed.
     */
    public function get_items_permissions_check();
}
