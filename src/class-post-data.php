<?php
/**
 * A class to handle Yoast post data.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension;

use Yoast\WP\SEO\Builders\Indexable_Social_Image_Trait;
use Yoast\WP\SEO\Helpers\Indexable_To_Postmeta_Helper;
use Yoast\WP\SEO\Repositories\Indexable_Repository;
use Yoast\WP\SEO\Helpers\Image_Helper;
use Yoast\WP\SEO\Helpers\Twitter\Image_Helper as Twitter_Image_Helper;
use Yoast\WP\SEO\Helpers\Open_Graph\Image_Helper as Open_Graph_Image_Helper;

/**
 * Post_Data class.
 *
 * Extends ORM because ::execute method is protected.
 */
class Post_Data {

    use Indexable_Social_Image_Trait;

    /**
     * Sets post data.
     *
     * @param int    $post_id  Post ID.
     * @param string $property Property.
     * @param mixed  $value    Value.
     *
     * @return bool
     *
     * @see Indexable
     */
    public function set_post_data( $post_id, $property, $value ) {
        $indexable_repository  = YoastSEO()->classes->get( Indexable_Repository::class );
        $indexable_to_postmeta = YoastSEO()->classes->get( Indexable_To_Postmeta_Helper::class );
        $indexable             = $indexable_repository->find_by_id_and_type( $post_id, 'post' );

        $boolean_types = [
            'is_robots_noindex',
            'is_robots_nofollow',
        ];
        if ( in_array( $property, $boolean_types, true ) ) {
            $false_strings = [ '0', 'false', 'none', 'no' ];

            if ( in_array( strtolower( $value ), $false_strings, true ) ) {
                $value = false;
            } else {
                $value = (bool) $value;
            }
        }

        $indexable->{$property} = $value;

        $images_mapping = [
            'twitter_image'    => 'twitter_image_id',
            'open_graph_image' => 'open_graph_image_id',
        ];

        if ( in_array( $property, array_keys( $images_mapping ), true ) ||
            in_array( $property, array_values( $images_mapping ), true )
        ) {
            $image_helper         = YoastSEO()->classes->get( Image_Helper::class );
            $og_image_helper      = YoastSEO()->classes->get( Open_Graph_Image_Helper::class );
            $twitter_image_helper = YoastSEO()->classes->get( Twitter_Image_Helper::class );

            if ( in_array( $property, array_keys( $images_mapping ), true ) ) { // Images by URLs.
                $image_id = $image_helper->get_attachment_by_url( $value );
                if ( ! $image_id ) {
                    wp_die( __( "Can't find image ID. Are you using the image from this website?", 'yoast-api-extension' ) );
                }
                $indexable->{$images_mapping[ $property ]} = (string) $image_id;
            }

            if ( in_array( $property, array_values( $images_mapping ), true ) ) { // Images by IDs.
                $image_url_field_name = array_flip( $images_mapping )[ $property ];

                $image_url = $image_helper->get_attachment_image_source( $value );
                if ( ! $image_url ) {
                    wp_die( __( "Can't find image URL by ID. Maybe it was deleted or the wrong ID? You can also use URL on '${$image_url_field_name}'", 'yoast-api-extension' ) );
                }
                $indexable->{$image_url_field_name} = $image_url;
            }

            $this->set_social_image_helpers( $image_helper, $og_image_helper, $twitter_image_helper );
            $this->handle_social_images( $indexable );
        }

        $indexable->save();
        $indexable_to_postmeta->map_to_postmeta( $indexable );

        return true;
    }
}
