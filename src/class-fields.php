<?php
/**
 * A class for fields handling.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension;

use Yoast\WP\SEO\Helpers\Indexable_To_Postmeta_Helper;

/**
 * Post_Data class.
 *
 * Extends Indexable_To_Postmeta_Helper because $yoast_to_postmeta variable is protected.
 */
class Fields extends Indexable_To_Postmeta_Helper {

    /**
     * The list of excluded fields.
     *
     * @var string[]
     */
    public $excluded_fields = [
        'meta_robots_adv',
    ];

    /**
     * Validates field name (whether such field exists)
     *
     * @param string $db_field_name DB field name.
     *
     * @return bool Whether such field exists and allowed.
     */
    public function yae_validate_field_name( $db_field_name ) {
        return in_array( $db_field_name, array_diff( array_keys( $this->yoast_to_postmeta ), $this->excluded_fields ), true );
    }

    /**
     * Returns supported fields list.
     *
     * @return string[] Supported field names.
     */
    public function yae_get_supported_fields_list() {
        $list = [];

        foreach ( array_diff( array_keys( $this->yoast_to_postmeta ), $this->excluded_fields ) as $name ) {
            $list[] = $name;
        }

        return $list;
    }

    /**
     * Constructor.
     *
     * @noinspection PhpMissingParentConstructorInspection
     */
    public function __construct() {}
}
