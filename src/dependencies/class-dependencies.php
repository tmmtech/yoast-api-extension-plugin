<?php
/**
 * Dependencies.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension\Dependencies;

/**
 * Class Dependencies.
 */
final class Dependencies {

    /**
     * The list of dependencies classes.
     */
    const DEPENDENCIES = [
        'Yoast_API_Extension\Dependencies\Yoast',
        'Yoast_API_Extension\Dependencies\WooCommerce',
    ];

    /**
     * The list of dependencies objects.
     *
     * @var Dependency[] Dependencies.
     */
    private $dependencies = [];

    /**
     * Constructor.
     */
    public function __construct() {
        foreach ( self::DEPENDENCIES as $dependency ) {
            $this->dependencies[] = new $dependency();
        }
    }

    /**
     * Adds all dependencies.
     */
    public function add() {
        foreach ( $this->dependencies as $dependency ) {
            $dependency->add();
        }
    }

    /**
     * Checks whether the plugins are active.
     *
     * @return bool Whether all dependency plugins are active.
     */
    public function are_plugins_active() {
        $are_plugins_active = true;

        foreach ( $this->dependencies as $dependency ) {
            if ( ! $dependency->is_plugin_active() || ! $dependency->is_plugin_file_exist() ) {
                $are_plugins_active = false;
                break;
            }
        }

        return $are_plugins_active;
    }

    /**
     * Checks whether the plugins are installed.
     *
     * @return bool Whether all plugins are installed.
     */
    public function are_plugins_installed() {
        $are_plugins_installed = true;

        foreach ( $this->dependencies as $dependency ) {
            if ( ! $dependency->is_plugin_file_exist() ) {
                $are_plugins_installed = false;
                break;
            }
        }

        return $are_plugins_installed;
    }
}
