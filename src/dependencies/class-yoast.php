<?php
/**
 * Makes Yoast a dependency (a requirement) to start the plugin.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension\Dependencies;

/**
 * Class Yoast Dependency.
 */
final class Yoast extends Dependency {

    /**
     * The plugin file.
     *
     * @var string
     */
    public $plugin_file = 'wordpress-seo/wp-seo.php';

    /**
     * The plugin name.
     *
     * @var string
     */
    public $plugin_name = 'Yoast SEO';
}
