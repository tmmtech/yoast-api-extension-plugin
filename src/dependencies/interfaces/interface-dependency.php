<?php
/**
 * Interface for a dependency.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension\Dependencies;

/**
 * Class WC Dependency.
 */
interface Dependency_Interface {

    /**
     * Init.
     */
    public function add();

    /**
     * WC is not active error notice.
     */
    public function plugin_not_active_admin_notice();

    /**
     * Checks whether the plugin is active.
     *
     * @return bool
     */
    public function is_plugin_active();
}
