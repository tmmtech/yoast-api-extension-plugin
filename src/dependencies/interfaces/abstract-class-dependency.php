<?php
/**
 * Abstract class for a dependency
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension\Dependencies;

use Yoast_API_Extension\Yoast_API_Extension;

/**
 * Class WC Dependency.
 */
abstract class Dependency implements Dependency_Interface {

    /**
     * The plugin file.
     *
     * @var string
     */
    public $plugin_file;

    /**
     * The plugin name.
     *
     * @var string
     */
    public $plugin_name;

    /**
     * Deactivates Yoast API Extension plugin.
     */
    public static function deactivate_yoast_api_extension_plugin() {
        deactivate_plugins( YOAST_API_EXTENSION_BASENAME );
    }

    /**
     * Adds the dependency.
     */
    public function add() {
        add_action(
            'init',
            function () {
                // Deactivates the plugin on activation if WC is not active.
                if (
                    is_admin() && current_user_can( 'activate_plugins' ) &&
                    ! is_plugin_active( $this->plugin_file )
                ) {
                    self::deactivate_yoast_api_extension_plugin();
                    if ( isset( $_GET['activate'] ) ) {
                        add_action(
                            'admin_notices',
                            [
                                $this,
                                'plugin_not_active_admin_notice',
                            ]
                        );
                        unset( $_GET['activate'] );
                    }
                }
            }
        );
    }

    /**
     * Checks whether the plugin file exists.
     *
     * @return bool
     */
    public function is_plugin_file_exist() {
        return Yoast_API_Extension::$fs->is_file( trailingslashit( WP_PLUGIN_DIR ) . $this->plugin_file );
    }

    /**
     * Checks whether the plugin is active.
     *
     * @return bool
     */
    public function is_plugin_active() {
        return in_array( $this->plugin_file, apply_filters( 'active_plugins', get_option( 'active_plugins' ) ), true );
    }

    /**
     * Plugin is not active error notice.
     */
    public function plugin_not_active_admin_notice() {
        echo '<div class="notice notice-error is-dismissible">
            <p>' .
            str_replace(
                '{}',
                $this->plugin_name,
                __( 'To activate Yoast API Extension plugin, you must have {} plugin active.', 'yoast_api_extension' )
            ) . '</p>
        </div>';
    }
}
