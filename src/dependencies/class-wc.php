<?php
/**
 * Makes WC a dependency (a requirement) to start the plugin.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension\Dependencies;

/**
 * Class WC Dependency.
 */
final class WooCommerce extends Dependency {

    /**
     * The plugin file.
     *
     * @var string
     */
    public $plugin_file = 'woocommerce/woocommerce.php';

    /**
     * The plugin name.
     *
     * @var string
     */
    public $plugin_name = 'WooCommerce';
}
