<?php
/**
 * Assets for main screen
 *
 * Loads assets (JS, CSS), adds data for them.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension\Assets\Menu\Screens;

/**
 * Assets class.
 */
final class Main {

    /**
     * Inits.
     */
    public static function init() {
        $class = __CLASS__;
        add_action(
            'admin_enqueue_scripts',
            function () use ( $class ) {
                new $class();
            }
        );
    }

    /**
     * Constructor.
     */
    public function __construct() {
        $this->styles();
        $this->scripts();
    }

    /**
     * Loads styles.
     */
    private function styles() {
        wp_register_style(
            'yoast-api-extension-admin-style',
            YOAST_API_EXTENSION_URL . 'assets-build/admin/index.css',
            [],
            YOAST_API_EXTENSION_VERSION
        );

        wp_enqueue_style(
            'yoast-api-extension-admin-main-screen-style',
            YOAST_API_EXTENSION_URL . 'assets-build/admin/screens/main.css',
            [ 'yoast-api-extension-admin-style', 'tmm-wp-plugins-core-admin-style' ],
            YOAST_API_EXTENSION_VERSION
        );
    }

    /**
     * Loads scripts.
     */
    private function scripts() {
        wp_register_script(
            'yoast-api-extension-admin-script',
            YOAST_API_EXTENSION_URL . 'assets-build/admin/index.js',
            [],
            YOAST_API_EXTENSION_VERSION,
            true
        );

        wp_enqueue_script(
            'yoast-api-extension-admin-main-screen-script',
            YOAST_API_EXTENSION_URL . 'assets-build/admin/screens/main.js',
            [ 'yoast-api-extension-admin-script', 'tmm-wp-plugins-core-admin-script' ],
            YOAST_API_EXTENSION_VERSION,
            true
        );
    }
}
