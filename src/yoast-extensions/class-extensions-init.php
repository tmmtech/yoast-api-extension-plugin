<?php
/**
 * Inits Yoast API extensions.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension\Yoast_Extensions;

/**
 * Class Init for init of Yoast API extensions.
 */
final class Init {

    /**
     * The list of extensions to add.
     */
    const EXTENSIONS_LIST = [
        'Yoast_API_Extension\Yoast_Extensions\WooCommerce\Init',
    ];

    /**
     * Adds the menu and inits assets loading for it.
     */
    public function __construct() {
        foreach ( self::EXTENSIONS_LIST as $extension ) {
            new $extension();
        }
    }

    /**
     * Returns documentation tabs content for all extensions.
     *
     * @return string
     *
     * @noinspection PhpUndefinedMethodInspection
     */
    public static function get_documentation_tabs_content() {
        $html = '';

        foreach ( self::EXTENSIONS_LIST as $extension ) {
            if ( $extension::is_plugin_active_and_available() ) {
                $html .= $extension::get_documentation_tab_content();
            }
        }

        return $html;
    }
}
