<?php
/**
 * Extension Init abstract class.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension\Yoast_Extensions;

use Yoast_API_Extension\Yoast_API_Extension;

/**
 * Extension_Init abstract class.
 */
abstract class Extension_Init implements Extension_Init_Interface {

    /**
     * Returns documentation tab content.
     *
     * @return string
     */
    public static function get_documentation_tab_content() {
        $reflector = new \ReflectionClass( static::class );
        $dir       = dirname( $reflector->getFileName() );

        $templates_dir               = path_join( $dir, 'templates' );
        $documentation_template_file = path_join( $templates_dir, 'documentation-tab-content.php' );
        if ( Yoast_API_Extension::$fs->is_dir( $templates_dir ) &&
            Yoast_API_Extension::$fs->is_file( $documentation_template_file )
        ) {
            ob_start();
            require $documentation_template_file;
            return ob_get_clean();
        } else {
            return '';
        }
    }

    /**
     * Checks whether the plugin is active and available (not deleted).
     *
     * @return bool Whether the plugin is active.
     */
    public static function is_plugin_active_and_available() {
        return
            in_array( static::class::EXTENSION_PLUGIN_NAME, apply_filters( 'active_plugins', get_option( 'active_plugins' ) ), true ) &&
            array_key_exists( static::class::EXTENSION_PLUGIN_NAME, get_plugins() );
    }
}
