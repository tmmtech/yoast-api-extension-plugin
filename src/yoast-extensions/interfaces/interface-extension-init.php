<?php
/**
 * Extension Init interface.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension\Yoast_Extensions;

/**
 * Extension_Init_Interface interface.
 */
interface Extension_Init_Interface {

    /**
     * Returns documentation tab content.
     *
     * @return string
     */
    public static function get_documentation_tab_content();
}
