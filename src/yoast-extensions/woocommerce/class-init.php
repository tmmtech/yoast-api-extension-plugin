<?php
/**
 * API Extension for WooCommerce Yoast extension.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension\Yoast_Extensions\WooCommerce;

use Yoast_API_Extension\Yoast_Extensions\Extension_Init;

/**
 * Class Admin_Menu.
 */
final class Init extends Extension_Init {

    /**
     * Plugin name to check whether the plugin is active.
     */
    const EXTENSION_PLUGIN_NAME = 'wpseo-woocommerce2/wpseo-woocommerce2.php';

    /**
     * REST API endpoints.
     */
    const REST_API_CONTROLLERS = [
        'Yoast_API_Extension\Yoast_Extensions\WooCommerce\REST_API_Controllers\Set_Post_Identifier',
        'Yoast_API_Extension\Yoast_Extensions\WooCommerce\REST_API_Controllers\Set_Post_Identifiers',
    ];

    /**
     * The list of whitelisted REST API namespaces.
     *
     * @var array
     */
    private $rest_api_whitelisted_namespaces = [];

    /**
     * Constructor.
     */
    public function __construct() {
        if ( self::is_plugin_active_and_available() ) {
            $this->import_files();
            $this->init();
        }
    }

    /**
     * Imports files for the extension.
     */
    private function import_files() {
        $src_files = [
            'yoast-extensions/woocommerce/class-post-identifiers',
            'yoast-extensions/woocommerce/rest-api-controllers/class-set-post-identifier',
            'yoast-extensions/woocommerce/rest-api-controllers/class-set-post-identifiers',
        ];
        foreach ( $src_files as $file ) {
            require_once YOAST_API_EXTENSION_DIR . 'src/' . $file . '.php';
        }
    }

    /**
     * Inits the extension.
     */
    private function init() {
        $this->init_rest_api();
    }

    /**
     * Inits REST API.
     */
    private function init_rest_api() {
        foreach ( self::REST_API_CONTROLLERS as $controller ) {
            $this->whitelist_rest_namespace( $controller::REST_NAMESPACE );
        }

        // Adds new WooSearch REST API endpoints.
        add_filter( 'woocommerce_rest_api_get_rest_namespaces', [ $this, 'add_controllers' ] );
    }

    /**
     * Extends WooCommerce REST API Controllers with custom controllers.
     *
     * @param array $controllers List of Namespaces and their controller classes.
     *
     * @return array
     */
    public function add_controllers( array $controllers ) {
        foreach ( self::REST_API_CONTROLLERS as $controller ) {
            $controllers[ $controller::REST_NAMESPACE ][ $controller::REST_BASE ] = $controller;
        }

        return $controllers;
    }

    /**
     * Whitelists custom REST namespace for WooCommerce WC_REST_Authentication->is_request_to_rest_api().
     *
     * @param string $rest_namespace REST Namespace.
     */
    private function whitelist_rest_namespace( $rest_namespace ) {
        static $filter_added = false;

        if ( ! in_array( $rest_namespace, $this->rest_api_whitelisted_namespaces, true ) ) {
            $this->rest_api_whitelisted_namespaces[] = $rest_namespace;
        }

        if ( ! $filter_added ) {
            $filter_added = true;
            add_filter( 'woocommerce_rest_is_request_to_rest_api', [ $this, 'is_request_to_rest_api_filter' ] );
        }
    }

    /**
     * Check if the request to our REST API.
     *
     * @param bool $is_request_to_rest_api_filter Whether the request is to REST API filter.
     *
     * @return bool
     */
    public function is_request_to_rest_api_filter( $is_request_to_rest_api_filter ) {
        if ( ! $is_request_to_rest_api_filter ) {
            if ( empty( $_SERVER['REQUEST_URI'] ) ) {
                return false;
            }

            $rest_prefix = trailingslashit( rest_get_url_prefix() );
            $request_uri = esc_url_raw( wp_unslash( $_SERVER['REQUEST_URI'] ) );

            foreach ( $this->rest_api_whitelisted_namespaces as $whitelisted_namespace ) {
                if ( false !== strpos( $request_uri, $rest_prefix . $whitelisted_namespace ) ) {
                    return true;
                }
            }
        }

        return $is_request_to_rest_api_filter;
    }
}
