<?php
/**
 * Custom REST API Controller for the setup of the Yoast product identifiers (GTIN, ISBN etc).
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension\Yoast_Extensions\WooCommerce\REST_API_Controllers;

use WP_Error;
use Yoast_API_Extension\REST_API_Controllers\REST_Endpoint;
use Yoast_API_Extension\Yoast_Extensions\WooCommerce\Post_Identifiers;

/**
 * Class Set_Post_Identifier.
 */
class Set_Post_Identifiers implements REST_Endpoint {

    /**
     * Namespace.
     *
     * @var string
     */
    const REST_NAMESPACE = 'yoast-api-extension/v1';

    /**
     * Endpoint name.
     *
     * @var string
     */
    const REST_BASE = 'set_post_identifiers';

    /**
     * Post type (for which permissions check will be done).
     *
     * @var string
     */
    protected $post_type = 'product';

    /**
     * Register the routes.
     */
    public function register_routes() {
        register_rest_route(
            self::REST_NAMESPACE,
            '/' . self::REST_BASE . '/(?P<post_id>[\d-]+)',
            [
                'methods'             => 'PUT',
                'args'                => [
                    'identifiers' => [
                        'required'    => true,
                        'type'        => 'object',
                        'description' => 'The type of the identifier to set.',
                    ],
                    'post_id'     => [
                        'required'    => true,
                        'type'        => 'number',
                        'description' => 'The ID of the post to set the identifier for.',
                    ],
                ],
                'callback'            => [
                    $this,
                    'set_identifiers',
                ],
                'permission_callback' => [
                    $this,
                    'get_items_permissions_check',
                ],
            ]
        );
    }

    /**
     * Check if a given request has access to read items.
     *
     * @return WP_Error|boolean
     */
    public function get_items_permissions_check() {
        if ( ! wc_rest_check_post_permissions( $this->post_type, 'create' ) ) {
            return new WP_Error(
                'woocommerce_rest_cannot_create',
                __( 'Sorry, you are not allowed to create resources.', 'woocommerce' ),
                [ 'status' => rest_authorization_required_code() ]
            );
        }
        return true;
    }

    /**
     * Returns the identifier, be it GTIN or whatever.
     *
     * @param \WP_REST_Request $request Where the body is an JSON object with the following values:
     *
     *  identifiers - Array of identifiers and their values.
     *  post_id     - The ID of the post to set the identifier for.
     *
     * @return array
     */
    public function set_identifiers( $request ) {
        $identifiers = $request->get_param( 'identifiers' );
        $post_id     = $request->get_param( 'post_id' );

        $post_identifiers = new Post_Identifiers();

        foreach ( $identifiers as $identifier_name => $identifier_value ) {
            $post_identifiers->set_post_identifier( $post_id, $identifier_name, $identifier_value );
        }

        return [];
    }
}
