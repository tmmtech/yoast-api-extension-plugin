<?php
/**
 * A helper class to manage posts identifiers identifiers (GTIN, ISBN etc).
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

namespace Yoast_API_Extension\Yoast_Extensions\WooCommerce;

use WPSEO_WooCommerce_Yoast_Tab;

/**
 * Class Post_Identifiers.
 */
class Post_Identifiers extends WPSEO_WooCommerce_Yoast_Tab {

    /**
     * Posts identifiers. Key is slug, value is name.
     *
     * @return string[]
     */
    public function get_posts_identifiers() {
        return $this->global_identifier_types;
    }

    /**
     * Sets the post identifiers.
     *
     * @param string|int $post_id         The ID of the post.
     * @param string     $identifier_type The type of the identifier to set.
     * @param string     $value           Value.
     *
     * @see: $this->global_identifier_types For the list of supported identifiers.
     */
    public function set_post_identifier( $post_id, $identifier_type, $value ) {
        if ( ! array_key_exists( $identifier_type, $this->global_identifier_types ) ) {
            wp_die( __( 'Unknown identifier:', 'yoast-api-extension' ) . ' ' . $identifier_type ); // @codingStandardsIgnoreLine
        }

        $post_global_identifiers = get_post_meta( $post_id, 'wpseo_global_identifier_values', true );

        if ( ! is_array( $post_global_identifiers ) ) {
            $post_global_identifiers = array_map(
                function() {
                    return ''; // Empty-out identifier values.
                },
                ( new Post_Identifiers() )->get_posts_identifiers()
            );
        }

        $post_global_identifiers[ $identifier_type ] = $value;

        update_post_meta( $post_id, 'wpseo_global_identifier_values', $post_global_identifiers );
    }
}
