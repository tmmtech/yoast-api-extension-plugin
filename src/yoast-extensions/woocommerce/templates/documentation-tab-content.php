<?php
/**
 * Documentation tab content.
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

use Yoast_API_Extension\Yoast_Extensions\WooCommerce\Post_Identifiers;

?>

<div class="wp-plugins-core-tab-content" data-tab-name="product-identifiers">
    <h2 class="wp-plugins-core-tab-heading"><?php esc_html_e( 'Product Identifiers — "Yoast SEO: WooCommerce" plugin.', 'yoast-api-extension' ); ?></h2>

    <div class="yoast-api-extension-row mb-30 mt-0 text-align-center">
        <h4 class="mt-0"><?php esc_html_e( '1. Create a product with the identifiers:', 'yoast-api-extension' ); ?></h4>
    </div>

    <div class="yoast-api-extension-row">
        <table class="table-ta-left">
            <thead>
            <tr>
                <th>
                    <?php esc_html_e( 'Command example to create a product with GTIN8 by specifying meta data.', 'yoast-api-extension' ); ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                        <pre>
curl -X POST https://example.com/wp-json/wc/v3/products \
    -u consumer_key:consumer_secret \
    -H "Content-Type: application/json" \
    -d '{
        "name": "Premium Quality",
        "type": "simple",
        "regular_price": "21.99",
        "description": "Pellentesque habitant morbi.",
        "short_description": "Pellentesque habitant.",
        "meta_data": [
            {
                "key": "wpseo_global_identifier_values",
                "value": {
                    "gtin8": "35",
                    "gtin12": "",
                    "gtin13": "",
                    "gtin14": "",
                    "isbn": "",
                    "mpn": ""
                }
            }
        ]
    }'
                        </pre>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="yoast-api-extension-row mb-30 mt-30 text-align-center">
        <h4><?php esc_html_e( '2. Modify an identifier for the existing product:', 'yoast-api-extension' ); ?></h4>
    </div>

    <div class="yoast-api-extension-row">
        <table>
            <thead>
            <tr>
                <th>
                    <?php esc_html_e( 'Method', 'yoast-api-extension' ); ?>
                </th>
                <th>
                    <?php esc_html_e( 'Syntax', 'yoast-api-extension' ); ?>
                </th>
                <th>
                    <?php esc_html_e( 'Data', 'yoast-api-extension' ); ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    PUT
                </td>
                <td>
                    /wp-json/yoast-api-extension/v1/set_post_identifier/6360
                </td>
                <td>
                    { 'identifier_type' => 'gtin8', 'value' => '35' }
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="yoast-api-extension-row mb-30 mt-30 text-align-center">
        <h4><?php esc_html_e( '3. Modify a set of identifiers for the existing product:', 'yoast-api-extension' ); ?></h4>
    </div>

    <div class="yoast-api-extension-row">
        <table>
            <thead>
            <tr>
                <th>
                    <?php esc_html_e( 'Method', 'yoast-api-extension' ); ?>
                </th>
                <th>
                    <?php esc_html_e( 'Syntax', 'yoast-api-extension' ); ?>
                </th>
                <th>
                    <?php esc_html_e( 'Data', 'yoast-api-extension' ); ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    PUT
                </td>
                <td>
                    /wp-json/yoast-api-extension/v1/set_post_identifiers/6360
                </td>
                <td>
                    [ 'identifiers' => ['gtin8' => '123', 'gtin12' => 'abc' ] ]
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <?php
    $supported_identifiers = ( new Post_Identifiers() )->get_posts_identifiers();
    if ( $supported_identifiers ) :
        ?>
        <div class="yoast-api-extension-row mb-30 mt-30 text-align-center">
            <h4><?php esc_html_e( 'Supported identifiers:', 'yoast-api-extension' ); ?></h4>
        </div>
        <div class="yoast-api-extension-row">
            <table>
                <tbody>
                <?php foreach ( $supported_identifiers as $identifier => $identifier_name ) : ?>
                    <tr>
                        <td>
                            <?php echo esc_html( $identifier ); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php endif; ?>
</div>
