<?php
/**
 * Template for admin menu
 *
 * @package Yoast-API-Extension
 * @since   0.0.1
 */

use Yoast_API_Extension\Yoast_Extensions\Init as Extensions;
use Yoast_API_Extension\Yoast_API_Extension;

?>

<div class="wrap">
    <h1 class="yoast-api-extension-header"><?php esc_html_e( 'Yoast API Extension Documentation', 'yoast-api-extension' ); ?></h1>

    <div class="yoast-api-extension-container">
        <div class="yoast-api-extension-column">
            <div class="wp-plugins-core-tabs-container">
                <div class="wp-plugins-core-tab-content" data-tab-name="basic-documentation">
                    <h2 class="wp-plugins-core-tab-heading"><?php esc_html_e( 'Basic documentation', 'yoast-api-extension' ); ?></h2>

                    <div class="yoast-api-extension-row mb-30 text-align-center">
                        <h4><?php esc_html_e( 'Modify a field for the existing product:', 'yoast-api-extension' ); ?></h4>
                    </div>

                    <div class="yoast-api-extension-row">
                        <table>
                            <thead>
                            <tr>
                                <th>
                                    <?php esc_html_e( 'Method', 'yoast-api-extension' ); ?>
                                </th>
                                <th>
                                    <?php esc_html_e( 'Syntax', 'yoast-api-extension' ); ?>
                                </th>
                                <th>
                                    <?php esc_html_e( 'Data', 'yoast-api-extension' ); ?>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    PUT
                                </td>
                                <td>
                                    /wp-json/yoast-api-extension/v1/set_post_fields/6360
                                </td>
                                <td>
                                    [ 'fields' => ['twitter_title' => 'test title', 'twitter_description' => 'test description' ] ]
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <?php
                    $supported_fields = Yoast_API_Extension::$fields->yae_get_supported_fields_list();
                    if ( $supported_fields ) :
                        ?>
                        <div class="yoast-api-extension-row mb-30 mt-30 text-align-center">
                            <h4><?php esc_html_e( 'Supported fields:', 'yoast-api-extension' ); ?></h4>
                        </div>
                        <div class="yoast-api-extension-row">
                            <table>
                                <tbody>
                                <?php foreach ( $supported_fields as $field ) : ?>
                                    <tr>
                                        <td>
                                            <?php echo esc_html( $field ); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php echo Extensions::get_documentation_tabs_content(); ?>

            <hr>
            <div class="yoast-api-extension-row mb-30 text-align-center">
                <h4>
                    <?php
                    echo str_replace(
                        '{WooCommerce REST API}',
                        '<a href="admin.php?page=wc-settings&tab=advanced&section=keys">WooCommerce REST API</a>',
                        esc_html__( 'Note: Use {WooCommerce REST API} keys for authorization.', 'yoast-api-extension' )
                    )
                    ?>
                </h4>
            </div>
        </div>
    </div>
</div>
